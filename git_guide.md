---
title: "Git, GitLab and RStudio"
output:
  html_document:
    theme: united
    keep_md: yes

---

## Checking Git is installed or not


```bash
git --version

```

```
## git version 2.17.1
```

## Then create an account in gitlab.

## Setup global variable for Git


```bash
git config --global user.name "mohang13"
git config --global user.email "mohangovindasamy1331@gmail.com"

```

## Basics

create your repository (folder)


```bash
mkdir first_repo
cd first_repo
```

Initialise git in this directory



```bash
git init
```


```bash
echo "my first git" > readme.txt
```


```bash
git status
```


 Ask git to track files

```bash
git add readme.txt

or

git add -A

```

To untrack


```bash

git rm --cached readme.txt
```

Commit tracking files


```bash
git commit -m "added readme.txt"
```

add 2 more files


```bash
touch file1.txt
touch file2.txt

echo "2nd line added" >> readme.txt
```

git add and commit 

reverse last commit


```bash
git reset --soft HEAD~
```

ignoring files

.gitignore

```bash
echo "*.jpg" > .gitignore
```



## Version Controll

Make multiple commits

- To jump to particular commit
- The "." after id is very important


```bash
git log --oneline
git checkout <log_id> .
```



## GitLab

- create new project
- copy remote


```bash
git remote add origins https://.....
```


```bash
git push -u origin master
```

-u to make origin the default remote directory every time we push


```bash
git pull
```


## Initalising in R Studio

- It's pretty easy
- Create new project in Gitlab
- Copy https://...
- In R Studio
  - New Project
  - version control, enter the copied link
  - Congratulations, you're done
  - You can add md file
  
  
## Branching

check which branch


```bash
git branch
```

create new branch


```bash
git branch new/new-feature

```

Make the new branch as current working branch


```bash
git checkout new/new-feature

git checkout -b new/new-feature

git push origin new/new-feature
```
output: 

Merge using GitLab instead of local git command (Recommended)
  - Click Compare
  - Then Create merge request
  - Merge


## Forking

- Go to https://gitlab.com/mohang13/learngit
- Click Fork Button
- Now you got this repository
- Use git clone to clone this repository to local by clicking clone and copy the https://...url



```bash
git clone https://gitlab.com/<your-gitlab-username>/learngit.git

```

- Now make change to the rmd file
- Add 

```bash
echo "- yourname" >> git_guide.Rmd

```

- Commit and push
- Now give pull request to me

## UseRs:

- Mohan Govindasamy
