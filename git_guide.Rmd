---
title: "Git, GitLab and RStudio"
output:
  prettydoc::html_pretty:
    theme: leonids
    highlight: github

---

## Checking Git is installed or not

```{r, engine='bash'}
git --version

```

## Then create an account in gitlab.

## Setup global variable for Git

```{r, engine='bash', eval = FALSE}
git config --global user.name "mohang13"
git config --global user.email "mohangovindasamy1331@gmail.com"

```

## Basics

create your repository (folder)

```{r, engine='bash', eval=FALSE}
mkdir first_repo
cd first_repo
```

Initialise git in this directory


```{r, engine='bash', eval=FALSE}
git init
```

```{r, engine='bash', eval=FALSE}
echo "my first git" > readme.txt
```

```{r, engine='bash', eval=FALSE}
git status
```


 Ask git to track files
```{r, engine='bash', eval=FALSE}
git add readme.txt

or

git add -A

```

To untrack

```{r, engine='bash', eval=FALSE}

git rm --cached readme.txt
```

Commit tracking files

```{r, engine='bash', eval=FALSE}
git commit -m "added readme.txt"
```

add 2 more files

```{r, engine='bash', eval=FALSE}
touch file1.txt
touch file2.txt

echo "2nd line added" >> readme.txt
```

git add and commit 

reverse last commit

```{r, engine='bash', eval=FALSE}
git reset --soft HEAD~
```

ignoring files

.gitignore
```{r, engine='bash', eval=FALSE}
echo "*.jpg" > .gitignore
```



## Version Controll

Make multiple commits

- To jump to particular commit
- The "." after id is very important

```{r, engine='bash', eval=FALSE}
git log --oneline
git checkout <log_id> .
```



## GitLab

- create new project
- copy remote

```{r, engine='bash', eval=FALSE}
git remote add origins https://.....
```

```{r, engine='bash', eval=FALSE}
git push -u origin master
```

-u to make origin the default remote directory every time we push

```{r, engine='bash', eval=FALSE}
git pull
```


## Initalising in R Studio

- It's pretty easy
- Create new project in Gitlab
- Copy https://...
- In R Studio
  - New Project
  - version control, enter the copied link
  - Congratulations, you're done
  - You can add md file
  
  
## Branching

check which branch

```{r, engine='bash', eval=FALSE}
git branch
```

create new branch

```{r, engine='bash', eval=FALSE}
git branch new/new-feature

```

Make the new branch as current working branch

```{r, engine='bash', eval=FALSE}
git checkout new/new-feature

git checkout -b new/new-feature

git push origin new/new-feature
```
output: 

Merge using GitLab instead of local git command (Recommended)
  - Click Compare
  - Then Create merge request
  - Merge


## Forking

- Go to https://gitlab.com/mohang13/learngit
- Click Fork Button
- Now you got this repository
- Use git clone to clone this repository to local by clicking clone and copy the https://...url


```{r, engine='bash', eval=FALSE}
git clone https://gitlab.com/<your-gitlab-username>/learngit.git


```

- Now make change to the rmd file
- Add 
```{r, engine='bash', eval=FALSE}
echo "- yourname" >> git_guide.Rmd


```

- Commit and push
- Now give pull request to me

## UseRs:

- Anand Lakshmanan
